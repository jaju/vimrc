execute pathogen#infect()
syntax on
filetype plugin indent on
set sw=4

map <C-\> :!ctags -R .<CR>
set nocompatible          " We're running Vim, not Vi!

" Load matchit (% to bounce from do to end, etc.)
runtime! macros/matchit.vim

augroup myfiletypes
" Clear old autocmds in group
autocmd!
" autoindent with two spaces, always expand tabs
autocmd BufNewFile,BufRead,BufEnter *.scss set ft=css
autocmd FileType ruby,eruby,yaml,css,xml,xul set ai sw=2 sts=2 et
autocmd BufEnter,BufRead,BufNew *.cpp,*.cc,*.c,*.h,*.hpp,*.C,*.H,*.gradle,*.java,*.js set cindent sw=4 sts=4 et
autocmd BufEnter,BufRead,BufNew *.js,*.json set cindent sw=2 sts=2 et
autocmd BufEnter,BufRead,BufNew ~/.spacemacs set ft=lisp
augroup END

autocmd BufEnter,BufNew *.md set expandtab
autocmd BufEnter,BufNew,BufReadPost *.md,*.yml set ts=2 sts=2 et
autocmd BufNewFile,BufRead,BufEnter *.json set ft=javascript
autocmd BufNewFile,BufRead,BufEnter *.clj,*.cljs,*.edn,*.boot set ft=clojure

" Let's remember some things, like where the .vim folder is.
if has("win32") || has("win64")
    let windows=1
    let vimfiles=$HOME . "/vimfiles"
    let sep=";"
else
    let windows=0
    let vimfiles=$HOME . "/.vim"
    let sep=":"
endif

autocmd BufNewFile,BufRead *.json set ft=javascript
au BufNewFile,BufRead *.gradle setf groovy
au BufRead,BufNewFile,BufEnter,BufReadPost *.html,*.htm,*.rhtml,*.js,*.html.erb set expandtab ts=2 sw=2 sts=2

au BufNew,BufNewFile,BufRead,BufEnter *.php,*.phtml set expandtab ts=4 sts=4 sw=4

set hlsearch

" clang_complete
let g:clang_auto_select=1
let g:clang_complete_auto=1
let g:clang_complete_copen=1
let g:clang_hl_errors=1
let g:clang_periodic_quickfix=0
let g:clang_snippets=1
let g:clang_snippets_engine="clang_complete"
let g:clang_conceal_snippets=1
let g:clang_exec="clang"
let g:clang_user_options=""
let g:clang_auto_user_options="path, .clang_complete"
let g:clang_use_library=1
let g:clang_sort_algo="priority"
let g:clang_complete_macros=1
let g:clang_complete_patterns=1
nnoremap <Leader>q :call g:ClangUpdateQuickFix()<CR>
" clang_complete

set hidden

set foldmethod=indent
set foldlevel=99

map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h

au BufNew,BufNewFile,BufEnter,BufReadPost *.st set syntax=xml
au BufNew,BufNewFile,BufEnter,BufReadPost **Python/*.st set syntax=python
au BufNew,BufNewFile,BufEnter,BufReadPost **Java/*.st set syntax=java
au BufNew,BufNewFile,BufEnter,BufReadPost **Cpp/*.st set syntax=c++

au BufNew,BufNewFile,BufEnter,BufReadPost *.html set shiftwidth=2
" autocmd vimenter * NERDTree
map <c-o> :NERDTreeToggle<CR>

" Clojure
au BufNew,BufNewFile,BufEnter,BufReadPost *.clj map <C-K> :%Eval<CR>
